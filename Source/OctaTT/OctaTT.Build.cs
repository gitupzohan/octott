// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class OctaTT : ModuleRules
{
	public OctaTT(ReadOnlyTargetRules Target) : base(Target)
	{
		PrivateDependencyModuleNames.AddRange(new string[] { "AnimGraphRuntime" });
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] 
			{ 
				"Core",
				"CoreUObject",
				"Engine",
				"InputCore",
				"HeadMountedDisplay",
				"EnhancedInput",
				"UMG"
				
			});

		PublicIncludePaths.AddRange(new string[]
		{
			"OctaTT/Animations",
			"OctaTT/Character",
			"OctaTT/Game",
			"OctaTT/Interfaces",
			"OctaTT/GameplayActors"
		});
	}
}
