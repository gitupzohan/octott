#include "Interactble.h"

#include "Components/BoxComponent.h"
#include "Components/WidgetComponent.h"

AInteractble::AInteractble()
{
	PrimaryActorTick.bCanEverTick = true;
	DefaultScene = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultSceneRoot"));
	SetRootComponent(DefaultScene);
	
	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	BoxCollision->SetupAttachment(RootComponent);
	BoxCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BoxCollision->SetCollisionObjectType(ECC_WorldDynamic);
	BoxCollision->SetCollisionResponseToAllChannels(ECR_Ignore);
	BoxCollision->SetCollisionResponseToChannel(ECC_GameTraceChannel2,ECR_Block);
	
	
	WidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("Widget"));
	WidgetComponent->SetupAttachment(RootComponent);
	WidgetComponent->SetVisibility(false);
}

void AInteractble::ShowWidget(bool NewVisible)
{
	if (WidgetComponent)
	{
		WidgetComponent->SetVisibility(NewVisible);
	}
}

ADoor::ADoor()
{
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Door"));
	StaticMesh->SetupAttachment(RootComponent);
	StaticMesh->SetCollisionResponseToChannel(ECC_Camera,ECR_Ignore);
	StaticMesh->SetCollisionResponseToChannel(ECC_GameTraceChannel2,ECR_Ignore);//interact
}

void ADoor::DoorRotation()
{
	const float CurrentYawRot = StaticMesh->GetRelativeRotation().Yaw;
	const FRotator NewRot = FRotator(0.f, FMath::FInterpTo(CurrentYawRot, TargetYawRot, 0.02f, InterpRotateSpeed), 0.f);
	StaticMesh->SetRelativeRotation(NewRot);

	if (FMath::IsNearlyEqual(CurrentYawRot, TargetYawRot, 2.f))
	{
		StaticMesh->SetRelativeRotation(FRotator(0.f, TargetYawRot, 0.f));
		GetWorldTimerManager().ClearTimer(RotationTimer);
	}
}

void ADoor::Interact()
{
	GetWorldTimerManager().SetTimer(RotationTimer, this, &ADoor::DoorRotation, 0.02f, true);
	TargetYawRot = bOpening ? 0.f : AngleDoorOpen;
	bOpening = bOpening ? false : true;
}

void AEmote::GetMontage(UAnimMontage*& Montage)
{
	if (CooldownActive)
	{
		Montage = nullptr;
	}
	else if (EmotionMontage)
	{
		FTimerHandle CooldownTimer;
		GetWorldTimerManager().SetTimer(CooldownTimer,this,&AEmote::CooldownPickup,EmotionMontage->GetPlayLength(),false);
		Montage = EmotionMontage;
		CooldownActive = true;
	}
}

void AEmote::CooldownPickup()
{
	CooldownActive = false;
}

APickup::APickup()
{
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Door"));
	StaticMesh->SetupAttachment(RootComponent);
	StaticMesh->SetCollisionResponseToChannel(ECC_Camera,ECR_Ignore);
	StaticMesh->SetCollisionResponseToChannel(ECC_GameTraceChannel2,ECR_Ignore);
	StaticMesh->SetCollisionResponseToChannel(ECC_Pawn,ECR_Ignore);
}

void APickup::Interact()
{
	SetLifeSpan(0.8f);
}

