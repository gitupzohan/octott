#pragma once

#include "CoreMinimal.h"
#include "InteractInterface.h"
#include "GameFramework/Actor.h"
#include "Interactble.generated.h"

class UWidgetComponent;
class USceneComponent;
class UBoxComponent;
class UStaticMeshComponent;

UCLASS(Blueprintable)
class OCTATT_API AInteractble : public AActor, public IInteractInterface
{
	GENERATED_BODY()

public:
	
	AInteractble();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category= Components)
	USceneComponent* DefaultScene = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category= Components)
	UWidgetComponent* WidgetComponent = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category= Components)
	UBoxComponent* BoxCollision = nullptr;
	
	UFUNCTION(BlueprintCallable)
	virtual void ShowWidget(bool NewVisible) override;
};

UCLASS()
class OCTATT_API ADoor : public AInteractble
{
	GENERATED_BODY()

public:
	ADoor();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category= Components)
	UStaticMeshComponent* StaticMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category= Rotation)
	bool bOpening = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category= Rotation)
	float TargetYawRot = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category= Rotation)
	float AngleDoorOpen = 120.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category= Rotation)
	float InterpRotateSpeed = 5.f;
	
	FTimerHandle RotationTimer;

	void DoorRotation();
	
	UFUNCTION(BlueprintCallable)
	virtual void Interact() override;
};

UCLASS()
class OCTATT_API  AEmote : public AInteractble
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= Montage)
	UAnimMontage* EmotionMontage = nullptr;

	bool CooldownActive = false;

	UFUNCTION(BlueprintCallable)
	virtual void GetMontage(UAnimMontage*& Montage) override;
	void CooldownPickup();
};

UCLASS()
class OCTATT_API  APickup : public AEmote
{
	GENERATED_BODY()

public:
	APickup();
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category= Components)
	UStaticMeshComponent* StaticMesh = nullptr;

	virtual void Interact() override;
};
