
#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InteractInterface.generated.h"

UINTERFACE(MinimalAPI, Blueprintable)
class UInteractInterface : public UInterface
{
	GENERATED_BODY()
};

class OCTATT_API IInteractInterface
{
	GENERATED_BODY()

public:
	virtual void ShowWidget(bool NewVisible);
	virtual void Interact() ;
	virtual void GetMontage(UAnimMontage*& Montage);
};
