// Copyright Epic Games, Inc. All Rights Reserved.

#include "OctaTTGameMode.h"
#include "OctaTTCharacter.h"
#include "UObject/ConstructorHelpers.h"

AOctaTTGameMode::AOctaTTGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
