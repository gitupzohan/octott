
#include "InteractInterface.h"

// Add default functionality here for any IInteractInterface functions that are not pure virtual.
void IInteractInterface::ShowWidget(bool NewVisible)
{
}

void IInteractInterface::Interact()
{
}

void IInteractInterface::GetMontage(UAnimMontage*& Montage)
{
}
