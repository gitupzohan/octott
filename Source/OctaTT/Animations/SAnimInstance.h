#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "SAnimInstance.generated.h"

class AOctaTTCharacter;

UCLASS()
class OCTATT_API USAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	virtual void NativeInitializeAnimation() override;

	virtual void NativeBeginPlay() override;

	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

	void IKFootTrace(float Distance, FName SocketName1,
		FName SocketName2,float& IKOffset, FVector& ImpactPoint, bool& bBlockFoot, FVector& Normal) const;
	void FootPlacement(float DeltaSeconds);
	void UpdateDeltaAimValues();
	void UpdateMovementValues();
	void UpdateRotation();
	void SetDesiredRotation();

	UPROPERTY(BlueprintReadWrite)
	AOctaTTCharacter* Character = nullptr;

	float RFootOffset = 0.f;
	float LFootOffset = 0.f;
	
	UPROPERTY(BlueprintReadWrite, Category= IK)
	float HipOffset = 0.f;
	UPROPERTY(BlueprintReadWrite, Category= IK)
	FRotator RFootRot = FRotator(0.f);
	UPROPERTY(BlueprintReadWrite, Category= IK)
	FRotator LFootRot = FRotator(0.f);
	UPROPERTY(BlueprintReadWrite, Category= IK)
	FVector RFootEffectorLocation = FVector(0);
	UPROPERTY(BlueprintReadWrite, Category= IK)
	FVector LFootEffectorLocation = FVector(0);

	UPROPERTY(BlueprintReadWrite, Category= Aim)
	float DeltaAimPitch = 0.f;
	UPROPERTY(BlueprintReadWrite, Category= Aim)
	float DeltaAimYaw = 0.f;
	UPROPERTY(BlueprintReadWrite, Category= Aim)
	float DeltaAimRoll = 0.f;

	UPROPERTY(BlueprintReadWrite, Category= Movement)
	float Speed = 0.f;
	UPROPERTY(BlueprintReadWrite, Category= Movement)
	float Direction = 0.f;
	UPROPERTY(BlueprintReadWrite, Category= Movement)
	bool bIsInAir = false;
	
	UPROPERTY(BlueprintReadWrite, Category= TurnInPlace)
	bool bCanTurn = false;
	UPROPERTY(BlueprintReadWrite, Category= TurnInPlace)
	bool Perform180 = false;
	UPROPERTY(BlueprintReadWrite, Category= TurnInPlace)
	float TurnYaw = 0.f;
	UPROPERTY(BlueprintReadWrite, Category= TurnInPlace)
	bool bInRotate = false;
	UPROPERTY(BlueprintReadWrite, Category= TurnInPlace)
	float CachedYaw = 0.f;
	UPROPERTY(BlueprintReadWrite, Category= TurnInPlace)
	float TargetYaw = 0.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float IKInterpSpeed = 15.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool DebugIKFootTrace = false;
};
