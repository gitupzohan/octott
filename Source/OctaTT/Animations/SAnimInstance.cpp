#include "SAnimInstance.h"

#include "KismetAnimationLibrary.h"
#include "OctaTTCharacter.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"


static const FName NAME_Angle(TEXT("Angle"));
static const FName NAME_DistanceCurve(TEXT("DistanceCurve"));
static const FName NAME_TurnInPlaceCurve(TEXT("TurnInPlace"));
static const FName NAME_RightFootBone(TEXT("foot_r"));
static const FName NAME_LeftFootBone(TEXT("foot_l"));
static const FName NAME_LeftBallBone(TEXT("ball_l"));
static const FName NAME_RightBallBone(TEXT("ball_r"));
static const FName NAME_RootBone(TEXT("root"));


void USAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();
	Character = Cast<AOctaTTCharacter>(TryGetPawnOwner());
}

void USAnimInstance::NativeBeginPlay()
{
	Super::NativeBeginPlay();
}

void USAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);
	if (!Character || DeltaSeconds == 0.0f)
	{
		return;
	}
	FootPlacement(DeltaSeconds);
	UpdateDeltaAimValues();
	UpdateMovementValues();
	UpdateRotation();
}

void USAnimInstance::IKFootTrace(float Distance, FName SocketName1, FName SocketName2, float& IKOffset, FVector& ImpactPoint, bool& bBlockFoot,
                                 FVector& Normal) const
{
	if (!Character)
	{
		return;
	}
///foot trace
	FHitResult HitResultFoot;
	FVector FootLoc = Character->GetMesh()->GetSocketLocation(SocketName1);
	FVector RootLoc = Character->GetMesh()->GetSocketLocation(NAME_RootBone);
	FVector MeshLoc = Character->GetMesh()->GetComponentLocation();
	FVector StartFoot = FVector(FootLoc.X, FootLoc.Y, RootLoc.Z + 70.f);
	FVector EndFoot = FVector(FootLoc.X, FootLoc.Y, MeshLoc.Z - 70.f);
	bBlockFoot = UKismetSystemLibrary::SphereTraceSingle(
		GetWorld(),
		StartFoot,
		EndFoot,
		5.f,
		TraceTypeQuery3,
		false, TArray<AActor*>{Character},
		DebugIKFootTrace ? EDrawDebugTrace::ForOneFrame : EDrawDebugTrace::None,
		HitResultFoot,
		true);
//ball trace
	FHitResult HitResultBall;
	FVector BallLoc = Character->GetMesh()->GetSocketLocation(SocketName2);
	FVector StartBall = FVector(BallLoc.X, BallLoc.Y, RootLoc.Z + 70.f);
	FVector EndBall = FVector(BallLoc.X, BallLoc.Y, MeshLoc.Z - 70.f);
	bool bBlockBall = UKismetSystemLibrary::SphereTraceSingle(
		GetWorld(),
		StartBall,
		EndBall,
		5.f,
		TraceTypeQuery3,
		false, TArray<AActor*>{Character},
		DebugIKFootTrace ? EDrawDebugTrace::ForOneFrame : EDrawDebugTrace::None,
		HitResultBall,
		true);
	
	FVector NormalSurface = HitResultBall.Normal;
	if (FMath::Abs(HitResultBall.Distance-HitResultFoot.Distance) > 10.f)
	{
		if (HitResultBall.Distance < HitResultFoot.Distance)
		{
			//check angle surface
			NormalSurface = HitResultBall.Normal;
			bBlockBall = FMath::Abs(NormalSurface.X) < 0.7f && FMath::Abs(NormalSurface.Y) < 0.7f && FMath::Abs(NormalSurface.Z) > 0.7f && bBlockBall;
			//set out param
			IKOffset = bBlockBall ? (HitResultBall.ImpactPoint - MeshLoc).Z - HipOffset : 0.f;
			ImpactPoint = bBlockBall ? HitResultBall.ImpactPoint : FVector(0.0);
			Normal = bBlockBall ? HitResultBall.ImpactNormal : FVector(0.0);
			bBlockFoot = bBlockBall;
		}
		else
		{
			//check angle surface
			NormalSurface = HitResultFoot.Normal;
			bBlockFoot = FMath::Abs(NormalSurface.X) < 0.7f && FMath::Abs(NormalSurface.Y) < 0.7f && FMath::Abs(NormalSurface.Z) > 0.7f && bBlockFoot;
			//set out param
			IKOffset = bBlockFoot ? (HitResultFoot.ImpactPoint - MeshLoc).Z - HipOffset - 2.f : 0.f;
			ImpactPoint = bBlockFoot ? HitResultFoot.ImpactPoint : FVector(0.0);
			Normal = bBlockFoot ? HitResultFoot.ImpactNormal : FVector(0.0);
		}
	}
	else
	{
		//check angle surface
		NormalSurface = HitResultFoot.Normal;
		bBlockFoot = FMath::Abs(NormalSurface.X) < 0.7f && FMath::Abs(NormalSurface.Y) < 0.7f && FMath::Abs(NormalSurface.Z) > 0.7f && bBlockFoot;
		//set out param
		IKOffset = bBlockFoot ? (HitResultFoot.ImpactPoint - MeshLoc).Z - HipOffset - 2.f : 0.f;
		ImpactPoint = bBlockFoot ? HitResultFoot.ImpactPoint : FVector(0.0);
		Normal = bBlockFoot ? HitResultFoot.ImpactNormal : FVector(0.0);
	}
}

void USAnimInstance::FootPlacement(float DeltaSeconds)
{
	if (!Character)
	{
		return;
	}

	const float Distance = Character->GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
	//Set Right Foot offset
	FVector RImpactPoint = FVector(0);
	float TargetRFootOffset = 0.f;
	bool RbBlock = false;
	FVector RNormal = FVector(0);
	IKFootTrace(Distance, NAME_RightFootBone,NAME_RightBallBone, TargetRFootOffset, RImpactPoint, RbBlock, RNormal);
	RFootOffset = FMath::FInterpTo(RFootOffset, TargetRFootOffset, DeltaSeconds, IKInterpSpeed);
	RFootEffectorLocation = FVector(RFootOffset * -1.f, 0.0, 0.0);

	//Set Left Foot offset
	FVector LImpactPoint = FVector(0);
	float TargetLFootOffset = 0.f;
	bool LbBlock = false;
	FVector LNormal = FVector(0);
	IKFootTrace(Distance, NAME_LeftFootBone,NAME_LeftBallBone, TargetLFootOffset, LImpactPoint, LbBlock, LNormal);
	LFootOffset = FMath::FInterpTo(LFootOffset, TargetLFootOffset, DeltaSeconds, IKInterpSpeed);
	LFootEffectorLocation = FVector(LFootOffset, 0.0, 0.0);

	//Set right foot rotation

	const FRotator TargetRRot = FRotator(UKismetMathLibrary::DegAtan2(RNormal.X, RNormal.Z) * -1.f, 0.f, UKismetMathLibrary::DegAtan2(RNormal.Y, RNormal.Z));
	RFootRot = FMath::RInterpTo(RFootRot, TargetRRot, DeltaSeconds, IKInterpSpeed);

	//Set left foot rotation
	const FRotator TargetLRot = FRotator(UKismetMathLibrary::DegAtan2(LNormal.X, LNormal.Z) * -1.f, 0.f, UKismetMathLibrary::DegAtan2(LNormal.Y, LNormal.Z));
	LFootRot = FMath::RInterpTo(LFootRot, TargetLRot, DeltaSeconds, IKInterpSpeed);
	
	//Set hip offset
	const float TargetHipOffset = FMath::Abs(Character->GetMesh()->GetComponentLocation().Z - FMath::Min(RImpactPoint.Z,LImpactPoint.Z)) * -1.f;
	if (RbBlock && LbBlock)
	{
		HipOffset = FMath::FInterpTo(HipOffset, TargetHipOffset, DeltaSeconds, IKInterpSpeed);
	}
}

void USAnimInstance::UpdateDeltaAimValues()
{
	if (!Character)
	{
		return;
	}
	const FRotator AimDeltaRot = UKismetMathLibrary::NormalizedDeltaRotator(
		Character->GetBaseAimRotation(), Character->GetActorRotation());
	DeltaAimPitch = AimDeltaRot.Pitch;
	DeltaAimRoll = AimDeltaRot.Roll;
	DeltaAimYaw = AimDeltaRot.Yaw;
}

void USAnimInstance::UpdateMovementValues()
{
	if (!Character)
	{
		return;
	}
	Speed = Character->GetVelocity().Size();
	Direction = UKismetAnimationLibrary::CalculateDirection(Character->GetVelocity(), Character->GetActorRotation());
	bIsInAir = Character->GetMovementComponent()->IsFalling();
	bCanTurn = Character->TurnRate == 0.f;
}

void USAnimInstance::UpdateRotation()
{
	if (GetCurveValue(NAME_TurnInPlaceCurve) == 0.f)
	{
		TurnYaw = UKismetMathLibrary::NormalizedDeltaRotator(Character->GetBaseAimRotation(),
		                                                     Character->GetActorRotation()).Yaw;
		Perform180 = FMath::Abs(TurnYaw) > 135.f;
	}
	else
	{
		if (!FMath::IsNearlyEqual(FMath::Abs(GetCurveValue(NAME_DistanceCurve)),FMath::Abs(GetCurveValue(NAME_Angle)), 1.f))
		{
			Character->GetCharacterMovement()->bUseControllerDesiredRotation = false;
			SetDesiredRotation();
		}
		else
		{
			bInRotate = false;
		}
	}
	Character->GetCharacterMovement()->bUseControllerDesiredRotation = Speed > 50.f;
}

void USAnimInstance::SetDesiredRotation()
{
	if (!bInRotate)
	{
		CachedYaw = Character->GetActorRotation().Yaw;
		TargetYaw = UKismetMathLibrary::ComposeRotators(FRotator(0.f, GetCurveValue(NAME_Angle), 0.f),
		                                                Character->GetActorRotation()).Yaw;
		bInRotate = true;
	}

	bool CanRotation = FMath::IsNearlyEqual(FMath::Abs(GetCurveValue(NAME_DistanceCurve)),
	                                        FMath::Abs(GetCurveValue(NAME_Angle)), 5.f);
	if (CanRotation)
	{
		TurnYaw = 0.f;
	}
	else
	{
		FRotator NewRot = UKismetMathLibrary::NormalizedDeltaRotator(
			FRotator(0.f, CachedYaw, 0.f),
			FRotator(0.f, GetCurveValue(NAME_DistanceCurve),0.f));
		Character->SetActorRotation(NewRot);
	}
}
