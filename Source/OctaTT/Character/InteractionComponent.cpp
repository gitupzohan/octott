
#include "InteractionComponent.h"

#include "InteractInterface.h"
#include "OctaTTCharacter.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/KismetSystemLibrary.h"

UInteractionComponent::UInteractionComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

}

void UInteractionComponent::BeginPlay()
{
	Super::BeginPlay();
	OwnerCharacter = Cast<AOctaTTCharacter>(GetOwner());

}

void UInteractionComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (!OwnerCharacter)
	{
		return;
	}

	const FVector Start = OwnerCharacter->GetCapsuleComponent()->GetComponentLocation();
	const FVector End = Start + OwnerCharacter->GetBaseAimRotation().Vector() * 100.f;
	FHitResult HitResult;
	UKismetSystemLibrary::CapsuleTraceSingle(GetWorld(),
		Start,
		End,
		OwnerCharacter->GetCapsuleComponent()->GetScaledCapsuleRadius(),
		OwnerCharacter->GetCapsuleComponent()->GetScaledCapsuleHalfHeight(),
		TraceTypeQuery4,
		false,
		TArray<AActor*>{OwnerCharacter},
		DebugTrace ? EDrawDebugTrace::ForOneFrame : EDrawDebugTrace::None,
		HitResult,
		true);

	if (HitResult.bBlockingHit)
	{
		if (HitActor == HitResult.GetActor() || !HitActor)
		{
			HitActor = HitResult.GetActor();
			IInteractInterface* InteractActor = Cast<IInteractInterface>(HitActor);
			if (InteractActor)
			{
				InteractActor->ShowWidget(true);
			}
		}
		else
		{
			IInteractInterface* InteractActor = Cast<IInteractInterface>(HitActor);
			if (InteractActor)
			{
				InteractActor->ShowWidget(false);
				HitActor = nullptr;
			}
		}
	}
	else
	{
		IInteractInterface* InteractActor = Cast<IInteractInterface>(HitActor);
		if (InteractActor)
		{
			InteractActor->ShowWidget(false);
			HitActor = nullptr;
		}
	}
}

