// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "OctaTTGameMode.generated.h"

UCLASS(minimalapi)
class AOctaTTGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AOctaTTGameMode();
};



